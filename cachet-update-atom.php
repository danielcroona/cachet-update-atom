#!/usr/bin/php
<?php
error_reporting( E_ALL );
ini_set('display_errors', '1');

require_once( 'html2markdown/html2markdown.php' );


if ($argc != 3) {
    echo 'Usage: ' . basename(__FILE__) . ' "cachet_component" Atom-URL' . "\n";
	exit(1);
}

function getBasePath() {
    list($scriptPath) = get_included_files();

    $parts = explode('/', $scriptPath );
    $void = array_pop($parts);
    $scriptPath = implode('/', $parts).'/';

    return $scriptPath;
}
$basepath = getBasePath();

if (file_exists($basepath.'config.php')):
    include($basepath.'config.php');
else:
    die('copy config_sample.php to config.php and modify according to README file');
endif;



$incident_prefix = '[Autocheck]';
$cachet_notify_subscribers = true;
$cachet_incident_visible = true;

$cachet_component = $argv[1];
$atom_url = $argv[2];

define('ATOM_EXPIRE_TIME', 21); //number of days to check

define('CACHET_STATUS_INVESTIGATING', 1);
define('CACHET_STATUS_IDENTIFIED', 2);
define('CACHET_STATUS_WATCHING', 3);
define('CACHET_STATUS_FIXED', 4);

define('CACHET_COMPONENT_STATUS_OPERATIONAL', 1);
define('CACHET_COMPONENT_STATUS_PERFORMANCE_ISSUES', 2);
define('CACHET_COMPONENT_STATUS_PARTIAL_OUTAGE', 3);
define('CACHET_COMPONENT_STATUS_MAJOR_OUTAGE', 4);

function cachet_query($api_part, $action = 'GET', $data = null) {
	global $api_key, $cachet_url;

    $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $cachet_url . $api_part);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    if (in_array($action, array('GET', 'POST', 'PUT'))) {
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $action);
	}

	
    if ($data !== null && is_array($data)) {
		$ch_data = http_build_query($data);
		if ($action == 'GET'):
			curl_setopt($ch, CURLOPT_URL, $cachet_url . $api_part.'?'.$ch_data);
		else:
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $ch_data);
			var_export($ch_data);
		endif;
	}

    $ch_headers = array(
		'X-Cachet-Token: ' . $api_key
	);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $ch_headers);
	curl_setopt($ch, CURLOPT_HEADER, false); // Don't return headers
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return body
	
	$http_body = curl_exec($ch);
	$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	
    return array('code' => $http_code, 'body' => json_decode($http_body));
}

function getCachetStatusFromAtomContent($content) {

    if (strstr(strtolower($content), '<strong>resolved</strong>'))
        return CACHET_STATUS_FIXED;

    if (strstr(strtolower($content), '<strong>identified</strong>'))
        return CACHET_STATUS_IDENTIFIED;

    if (strstr(strtolower($content), '<strong>investigating</strong>'))
        return CACHET_STATUS_INVESTIGATING;

    if (strstr(strtolower($content), 'resolved'))
        return CACHET_STATUS_FIXED;

    if (strstr(strtolower($content), 'identified'))
        return CACHET_STATUS_IDENTIFIED;

    if (strstr(strtolower($content), 'investigating'))
        return CACHET_STATUS_INVESTIGATING;


    return CACHET_STATUS_FIXED;
}

function getCachetItemId( $prefix, $name, $diff_status=false ) {

    /* Get the incident ID */
    $data = array();
    $data['sort'] = 'id';
    $data['order'] = 'desc';
    $data['per_page'] = 1000;
    
	$results = cachet_query('incidents', 'GET', $data);
	if ($results['code'] != 200) {
		echo 'Can\'t get incidents' . "\n";
		exit(1);
	}
	
	$fuzzymatch_lastdate = strtotime('now -1 month');
	$fuzzymatch_id = 0;
	
	foreach ( $results['body']->data as $incident ) {
		
        if ( strtotime($incident->created_at) < strtotime(sprintf("now -%d days", ATOM_EXPIRE_TIME))):
        	echo $incident->name." to old (".$incident->created_at."), moving to next\n";
            continue;
        endif;

		if ( $incident->name == $prefix.' '.$name ) {
			return $incident->id;
		} elseif ( strstr($incident->name, $prefix) && strtotime($incident->created_at) > $fuzzymatch_lastdate ) {
            if ((!$diff_status || ($incident->status != $diff_status))) {
        		return 0;
            }
	    	$fuzzymatch_id = $incident->id;
	    	$fuzzymatch_lastdate = strtotime($incident->created_at);
		}
	}

	if ($fuzzymatch_id > 0) {
		return $fuzzymatch_id;
	}
	
	return false;
}


function writeIncidentData( $name, $message, $status, $visible, $component_id, $notify, $id=false ) {

    /* Update the incident */
	$query = array();
    if (!empty($name)) $query['name'] = $name;
    if (!empty($message)) $query['message'] = $message;
    if (!empty($status)) $query['status'] = $status;
    if (!empty($visible)) $query['visible'] = $visible;
    if (!empty($component_id)) $query['component_id'] = $component_id;
    if (!empty($status)) {
        $component_status = ($status == CACHET_STATUS_FIXED ? CACHET_COMPONENT_STATUS_OPERATIONAL : CACHET_COMPONENT_STATUS_PARTIAL_OUTAGE);
        $query['component_status'] = $component_status;
    }
    if (!empty($notify)) $query['notify'] = $notify;

    if ($id) {
    	$result = cachet_query('incidents/' . $id, 'PUT', $query);
    	if ($result['code'] != 200) {
    		echo 'Can\'t update incident' . "\n";
    		exit(1);
    	}
    } else {
        $result = cachet_query('incidents', 'POST', $query);
    	if ($result['code'] != 200) {
    		echo 'Can\'t create incident' . "\n";
    		exit(1);
    	}
    }

}



/* Find The ID of the Cachet component */
$result = cachet_query('components');
if ($result['code'] != 200) {
	echo 'Can\'t query components' . "\n";
	exit(1);
}
$cachet_component_id = false;
foreach ($result['body']->data as $component) {
	if ($cachet_component == $component->name) { // We nailed it
		$cachet_component_id = $component->id;
		break; // Yes, bad.
	}
}
if ($cachet_component_id === false) {
	echo 'Can\'t find component "' . $cachet_component . '"' . "\n";
	exit(1);
}


//GET CONTENTS FROM THE ATOM FEED
$feed = implode(file($atom_url));
$xml = simplexml_load_string($feed);
$json = json_encode($xml);
$historyItems = json_decode($json,TRUE);


if (!empty($historyItems['entry'])):
    foreach ($historyItems['entry'] as $item) {
        echo "\n---------\n";
        /*
        [id] => tag:status.klarna.com,2005:Incident/1655457
        [published] => 2018-03-23T13:38:22+01:00
        [updated] => 2018-03-23T13:38:22+01:00
        [link] => Array
            (
                [@attributes] => Array
                    (
                        [rel] => alternate
                        [type] => text/html
                        [href] => https://status.klarna.com/incidents/v9lh6j4d1js3
                    )

            )

        [title] => Klarna Checkout service issue in Nordic, DACH and the Netherlands countries
        [content] => <p><small>Mar 23, 13:38 CET</small><br><strong>Resolved</strong> - The issue is resolved. We are back to normal operation.</p><p><small>Mar 23, 13:08 CET</small><br><strong>Monitoring</strong> - The service returned to normal. We will continue monitoring the system.</p><p><small>Mar 23, 12:48 CET</small><br><strong>Investigating</strong> - We are currently experiencing an issue with the Klarna Checkout service in the live environment. We are investigating the issue.</p>
        */

        //if this isn't new, don't even bother
        if (strtotime($item['updated']) < strtotime(sprintf('now -%d days', ATOM_EXPIRE_TIME)))
            continue;

        $id = $item['id'];
        $publishtime = $item['published'];
        $updatetime = $item['published'];

        printf("Publishtime: %s\n", $item['published']);
        //$prefix = date('Y-m-d H:i', strtotime($publishtime));
        $prefix = $cachet_component;
        printf("Prefix: %s\n", $prefix);

        $link = false;
        if (!empty($item['link']['@attributes']['href'])):
            $link = $item['link']['@attributes']['href'];
        endif;

        $title = $item['title'];
        $item['content'] = str_replace( "</p>", "</p>\n\n", $item['content'] );
        $content = strip_tags($item['content'], '<strong><br><h1><h2><h3><h4><h5><h6><ul><ol><li><a><img><hr>');
        $content = html2markdown( $content );

        $status = getCachetStatusFromAtomContent($content);

        if ($status == CACHET_STATUS_FIXED) {
            echo "Status: Fixed\n";

            //check if there is an current issue to set as fixed
            $cachet_id = getCachetItemId( $prefix, $title, $status );

            if ($cachet_id) {
                //update with new status
                writeIncidentData( $prefix.' '.$title, $content, $status, $cachet_incident_visible, $cachet_component_id, $cachet_notify_subscribers, $cachet_id );
                printf("Updated incident: %s\n", $prefix.' '.$title);
            }

        } else {
            echo "Status: Not fixed\n";

            //check if there is a ticket to update
            $cachet_id = getCachetItemId( $prefix, $title, $status );

            if ($cachet_id) {
                //if the ticket has other content or status, run update
                writeIncidentData( $prefix.' '.$title, $content, $status, $cachet_incident_visible, $cachet_component_id, $cachet_notify_subscribers, $cachet_id );
                printf("Updated incident: %s\n", $prefix.' '.$title);
            } elseif ($cachet_id === false) {
                //create incident
                writeIncidentData( $prefix.' '.$title, $content, $status, $cachet_incident_visible, $cachet_component_id, $cachet_notify_subscribers );
                printf("Created incident: %s\n", $prefix.' '.$title);
            }
        }
        echo "---------\n";
        //var_dump($item);
    }
endif;



exit(0);
