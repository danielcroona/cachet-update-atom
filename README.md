# cachet-update-atom
A script for batch-updating a Cachet installation from remote serverstatus Atom feeds.

## Prerequisites
- Have a working Cachet installation
- Have some Cachet components created
- Have PHP CLI and php-curl installed

## Installation
- Get a Cachet API key: Create a new user in Cachet dashboard, login with this user, and get the API key in his profile.
- Change URL and API key in cachet-update-atom source code
- Make the script executable with `chmod +x ./cachet-update-atom.php`

## Run the script
`./cachet-update-atom.php`
